terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}



// Set variables
variable "yandex_token" {
    description = "Yandex Cloud security OAuth token"
    default     = "" #generate yours by this https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token
    type = string
    sensitive = true
}
variable "yandex_cloud_id" {
    description = "Yandex Cloud ID where resources will be created"
    default     = ""
    type = string
    sensitive = true
}
variable "yandex_folder_id" {
    description = "Yandex Cloud Folder ID where resources will be created"
    default     = ""
    type = string
}



// Configure Yandex.Cloud provider
provider "yandex" {
  token     = var.yandex_token
  cloud_id  = var.yandex_cloud_id
  folder_id = var.yandex_folder_id
  zone      = "ru-central1-a"
}



// Create a Web server instance
resource "yandex_compute_instance" "web-m11" {
  name = "web-m11"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet-m11.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

}



// Create a BD server instance
resource "yandex_compute_instance" "bd-m11" {
  name = "bd-m11"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet-m11.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

}



// Create a Monitoring server instance
resource "yandex_compute_instance" "track-m11" {
  name = "track-m11"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"

  scheduling_policy {
    preemptible = "true"
  }

  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }

  boot_disk {
    initialize_params {
      image_id = "fd82sqrj4uk9j7vlki3q"
      type = "network-hdd"
      size = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vpc-subnet-m11.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }

  provisioner "file" {
    source      = "user_data.sh"
    destination = "/tmp/user_data.sh"
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/id_ed25519")}"
      host        = "${yandex_compute_instance.track-m11.network_interface.0.nat_ip_address}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/user_data.sh",
      "/tmp/user_data.sh",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/id_ed25519")}"
      host        = "${yandex_compute_instance.track-m11.network_interface.0.nat_ip_address}"
    }
  }

}



// Configure VPC Nework
resource "yandex_vpc_network" "vpc-network-m11" {
  name = "vpc-network-m11"
}
resource "yandex_vpc_subnet" "vpc-subnet-m11" {
  name           = "vpc-subnet-m11"
  zone           = "ru-central1-a"
  network_id     = "${yandex_vpc_network.vpc-network-m11.id}"
  v4_cidr_blocks = ["10.241.1.0/24"]
}



// Show internal IP
output "internal_ip_address_vm_1" {
  value = "${yandex_compute_instance.web-m11.network_interface.0.ip_address}"
}
output "internal_ip_address_vm_2" {
  value = "${yandex_compute_instance.bd-m11.network_interface.0.ip_address}"
}
output "internal_ip_address_vm_3" {
  value = "${yandex_compute_instance.track-m11.network_interface.0.ip_address}"
}

// Show external IP
output "external_ip_address_vm_1" {
  value = "${yandex_compute_instance.web-m11.network_interface.0.nat_ip_address}"
}
output "external_ip_address_vm_2" {
  value = "${yandex_compute_instance.bd-m11.network_interface.0.nat_ip_address}"
}
output "external_ip_address_vm_3" {
  value = "${yandex_compute_instance.track-m11.network_interface.0.nat_ip_address}"
}



// Create hosts file
resource "local_file" "inventory" {
  filename = "../ansible/hosts"
  content = <<EOF
[web_server]
web_1 ansible_host=${yandex_compute_instance.web-m11.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519

[bd_server]
bd_1 ansible_host=${yandex_compute_instance.bd-m11.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519

[track_server]
track_1 ansible_host=${yandex_compute_instance.track-m11.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519
  EOF
}



// Create prometheus.yml file
resource "local_file" "inventory" {
  filename = "../files/prometheus.yml"
  content = <<EOF
# Global config
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  external_labels:
      monitor: 'example'

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets: ['localhost:9093']

# Load rules once and periodically evaluate them according to the global 'evaluation_interval'.
rule_files:
  - rules.yml

# A scrape configuration containing exactly one endpoint to scrape:
scrape_configs:
  - job_name: track-m11-prometheus
    static_configs:
      - targets: ['localhost:9090']
  - job_name: track-m11-node-exporter
    static_configs:
      - targets: ['localhost:9100']
  - job_name: web-m11-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.web-m11.network_interface.0.nat_ip_address}:9100']
  - job_name: web-m11-nginx-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.web-m11.network_interface.0.nat_ip_address}:9113']
  - job_name: bd-m11-node-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.bd-m11.network_interface.0.nat_ip_address}:9100']
  - job_name: bd-m11-mysql-exporter
    static_configs:
      - targets: ['${yandex_compute_instance.bd-m11.network_interface.0.nat_ip_address}:9104']
  EOF
}